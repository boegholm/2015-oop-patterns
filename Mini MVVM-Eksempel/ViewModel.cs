﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMWorkshop
{
    class ExampleViewModel : INotifyPropertyChanged //, INotifyPropertyChanging
    {
        public event PropertyChangedEventHandler PropertyChanged;

        String _myString;
        public String StringProperty
        {
            get { return _myString; }
            set
            {
                _myString = value;
                OnPropertyChanged("StringProperty");
            }
        }


        #region OnPropertyChanged Implementation
        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
        #endregion


        #region ObservableCollection

        private ObservableCollection<string> collectionProperty = new ObservableCollection<String>();
        public ObservableCollection<string> ColletionProperty { get { return collectionProperty; } private set{collectionProperty = value;} }

        #endregion


        #region AddTextToCollectionCommand

        #region ActionCommand
        private class ActionCommand : ICommand
        {
            Action<Object> action;
            public ActionCommand(Action<Object> action)
            {
                this.action = action;
            }

            public event EventHandler CanExecuteChanged;
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public void Execute(object parameter)
            {
                action(parameter);
            }
        }
        #endregion

        private ICommand ac;
        public ICommand AddTextToCollectionCommand
        {
            get{
                if(ac==null)
                    ac = new ActionCommand((str) => collectionProperty.Add((String)str));
                return ac;
            }
            set { }
        }
        #endregion
    }
    
}
