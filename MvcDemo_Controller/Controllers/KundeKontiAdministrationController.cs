﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcDemo_Model.Models;

namespace MvcDemo_Controller.Controllers
{
    public class KundeKontiAdministrationController
    {
        public KundeKontiAdministrationController(Kunde aktuelKunde)
        {
            this.Kunde = aktuelKunde;
        }

        public Kunde Kunde { get; private set; }

        //I denne udgave a 'OpretKonto' er det viewets ansvar at konstruere et konto objekt
        public void OpretKonto(Konto nyKonto)
        {
            Kunde.TilføjKonto(nyKonto);
        }

        public void OpretKonto(int kontoNummer, decimal saldo)
        {
            this.OpretKonto(new Konto(kontoNummer) { Saldo = saldo });
        }

        //I denne udgave af 'OpretKonto' tager controlleren sig af at konstruere et konto objekt
        public void OpretKonto(string kontoNummer, string saldo)
        {
            int gyldigtKontoNummer;
            decimal gyldigSaldo;
            if (!int.TryParse(kontoNummer, out gyldigtKontoNummer))
                throw new ArgumentException("Kontonummer er ikke et gyldigt heltal");
            if (!decimal.TryParse(saldo, out gyldigSaldo))
                throw new ArgumentException("Saldo er ikke et gyldigt beløb");

            OpretKonto(new Konto(gyldigtKontoNummer) { Saldo = gyldigSaldo });
        }

        public bool SletKonto(Konto konto)
        {
            return Kunde.FjernKonto(konto);
        }

        public IEnumerable<Konto> VisKonti()
        {
            return Kunde.Konti;
        }
    }
}
