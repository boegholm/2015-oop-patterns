﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using MvcDemo_Model.Models;

namespace MvcDemo_Controller.Controllers
{
    public class KundeAdministrationController: INotifyCollectionChanged
    {
        private KundeContainer _Kundeliste;
                
        public KundeAdministrationController()
        {
            _Kundeliste = KundeContainer.Instance;
        }
        
        public IEnumerable<Kunde> VisAlleKunder()
        {
            return _Kundeliste;
        }

        public IEnumerable<Kunde> VisRigeKunder()
        {
            //Aflæsning med 'Medium' kompleksitet
            foreach (Kunde kunde in _Kundeliste)
            {
                foreach (Konto konto in kunde.Konti)
                {
                    if (konto.Saldo > 5000)
                    {
                        yield return kunde;
                    }
                }
            }
        }

        //I dette scenarie modtager controlleren 'rå' data fra brugergrænsefladen og konstruerer
        //kunde objekt.
        public void OpretKunde(string kundenavn, string kundenummer)
        {
            //Evt. transformering af råt input til passende data type.
            Kunde k = new Kunde(kundenavn, kundenummer);
            OpretKunde(k);
        }

        //I dette scenarie er brugergrænsefladen selv ansvarlig for at konstruere kunde objektet
        public void OpretKunde(Kunde k)
        {
            //Validering der kræver kendskab til problemdomæne-logik skal foretages i controller
            //(Nedenstående validering foretages desuden i selve kunde-containeren)
            if (_Kundeliste.ContainsKundeWithKundenummer(k.KundeNummer))
                throw new ArgumentException("Kunde med det angivne kundenummer findes allerede");

            _Kundeliste.Add(k);
        }

        public bool SletKunde(Kunde k)
        {
            return _Kundeliste.Remove(k);
        }

        //Når et view subscriber til CollectionChanged event på denne controller
        //subscriber viewet i virkeligheden til modellens CollectionChanged event. 
        //Alternativt, kunne view derfor også ligeså godt have subscribed direkte på Model.KundeContainer
        //Fordel ved en controller-løsning er at man kan samle alle events ét sted frem for at et 
        //view skal subscribe til events på (mange) forskellige model-klasser.
        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add { _Kundeliste.CollectionChanged += value; }
            remove { _Kundeliste.CollectionChanged -= value; }
        }
    }
}
