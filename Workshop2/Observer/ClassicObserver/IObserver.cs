namespace Workshop2.Observer.ClassicObserver
{
    public interface IObserver
    {
        void Update(ISubject theChangedSubject);
    }

    class ConcreteSubject : SubjectBase
    {
        private string _state;

        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                Notify();
            }
        }
    }
}