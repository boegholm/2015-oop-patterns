using System.Collections.Generic;

namespace Workshop2.Observer.ClassicObserver
{
    public abstract class SubjectBase : ISubject
    {
        List<IObserver> Observers { get; } = new List<IObserver>();

        public void Attach(IObserver observer) => Observers.Add(observer);

        public void Detach(IObserver observer) => Observers.Remove(observer);

        protected void Notify()
        {
            foreach (IObserver observer in Observers)
            {
                observer.Update(this);
            }
        }
    }
}