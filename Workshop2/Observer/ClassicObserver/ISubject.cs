﻿namespace Workshop2.Observer.ClassicObserver
{
    public interface ISubject
    {
        void Attach(IObserver observer);
        void Detach(IObserver observer);
    }
}