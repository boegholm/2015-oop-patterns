using System.Threading;

namespace Workshop2.Observer.ClassicObserver.Example
{
    public class ObservableStopWatch : SubjectBase
    {
        bool _running = true;
        int _value =0;

        public ObservableStopWatch()
        {
            Thread thread = new Thread(Count);
            thread.Start();
        }

        void Count()
        {
            while (_running)
            {
                _value++;
                Notify();
                Thread.Sleep(1000);
            }
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public void Stop()
        {
            _running = false;
        }
    }
}