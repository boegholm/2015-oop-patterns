﻿using System;

namespace Workshop2.Observer.ClassicObserver.Example
{
    class ConsoleWriterObserver : IObserver
    {
        public void Update(ISubject theChangedSubject)
        {
            Console.WriteLine(theChangedSubject);
        }
    }
}