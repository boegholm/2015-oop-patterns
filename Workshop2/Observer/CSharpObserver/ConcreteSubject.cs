using System;

namespace Workshop2.Observer.CSharpObserver
{
    class ConcreteSubject
    {
        public event Action<ConcreteSubject> OnStateChanged;
        private string _state;
        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                Action<ConcreteSubject> action = OnStateChanged;
                if (action != null)
                    action(this);
            }
        }
    }
}