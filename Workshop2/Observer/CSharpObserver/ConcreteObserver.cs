using System;

namespace Workshop2.Observer.CSharpObserver
{
    class ConcreteObserver 
    {
        private string _name;
        public ConcreteObserver(ConcreteSubject subject, string name)
        {
            this._name = name;
            subject.OnStateChanged += Update;
        }

        public void Update(ConcreteSubject subject)
        {
            //En nyhed tikker ind...
            //Vi sp�rger subject hvad den nye tilstand er
            Console.WriteLine("{0}�registered:�{1}", this._name, subject.State);
            
        }
    }
}