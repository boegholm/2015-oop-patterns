namespace Workshop2.Command
{
    public abstract class KeyCommand : ICommand
    {
        protected readonly UndoRedoTextbox Textbox;
        protected readonly int SelectionStart;
        protected readonly int SelectLength;
        protected readonly string SelectedText;

        protected void RestorePosition()
        {
            Textbox.SelectionStart = SelectionStart;
            Textbox.SelectionLength = SelectLength;
        }

        protected KeyCommand(UndoRedoTextbox textbox)
        {
            this.Textbox = textbox;
            this.SelectionStart = textbox.SelectionStart;
            this.SelectLength = textbox.SelectionLength;
            this.SelectedText = textbox.SelectedText;
        }

        public abstract bool Execute();
        public abstract bool Undo();
    }
}