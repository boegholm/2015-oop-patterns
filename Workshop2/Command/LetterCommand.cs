using System.Windows.Forms;

namespace Workshop2.Command
{
    public class LetterCommand : KeyCommand
    {
        protected readonly string Data;

        public LetterCommand(UndoRedoTextbox textbox, Keys keyData)
            : base(textbox)
        {
            Data = keyData.ToString().ToLower();
        }

        public override bool Execute()
        {
            RestorePosition();
            Textbox.SelectedText = Data;
            return true;
        }

        public override bool Undo()
        {
            Textbox.SelectionStart = SelectionStart;
            Textbox.SelectionLength = Data.Length;
            Textbox.SelectedText = SelectedText;
            return true;
        }
    }
}