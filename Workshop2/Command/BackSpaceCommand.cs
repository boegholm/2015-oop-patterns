using System;

namespace Workshop2.Command
{
    public class BackSpaceCommand : KeyCommand
    {
        string _oldtext;
        int _newposition;
        public BackSpaceCommand(UndoRedoTextbox textbox) : base(textbox) { }
        public override bool Execute()
        {
            RestorePosition();
            if (SelectedText.Length == 0)
            {
                if (Textbox.SelectionStart > 0)
                {
                    Textbox.SelectionStart--;
                    Textbox.SelectionLength = 1;
                    _oldtext = Textbox.SelectedText;
                    Textbox.SelectedText = "";
                    _newposition = Textbox.SelectionStart;
                    return true;
                }
            }
            else
            {
                _oldtext = Textbox.SelectedText;
                Textbox.SelectedText = "";
                return true;
            }
            return false;
        }

        public override bool Undo()
        {
            Textbox.SelectionStart = _newposition;
            Textbox.SelectionLength = 0;
            Textbox.SelectedText = _oldtext;
            return true;
        }
    }
}