﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Workshop2.Command
{
    public partial class UndoRedoTextbox : TextBox
    {
        #region ICommand Execution

        private Stack<ICommand> performStack = new Stack<ICommand>();
        private Stack<ICommand> redoStack = new Stack<ICommand>();

        public void Execute(ICommand c)
        {
            bool result = c.Execute();
            if (result)
            {
                performStack.Push(c);
                redoStack.Clear();
            }
        }
        public void Redo()
        {
            if (redoStack.Count > 0)
            {
                ICommand c = redoStack.Pop();
                c.Execute();
                performStack.Push(c);
            }
        }
        public new void Undo()
        {
            if (performStack.Count > 0)
            {
                ICommand c = performStack.Pop();
                c.Undo();
                redoStack.Push(c);
            }
        }
        #endregion

        #region Key processing
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            switch (keyData)
            {
                case Keys.A:
                case Keys.B:
                case Keys.C:
                case Keys.D:
                case Keys.E:
                case Keys.F:
                case Keys.G:
                case Keys.H:
                case Keys.I:
                case Keys.J:
                case Keys.K:
                case Keys.L:
                case Keys.M:
                case Keys.N:
                case Keys.O:
                case Keys.P:
                case Keys.Q:
                case Keys.R:
                case Keys.S:
                case Keys.T:
                case Keys.U:
                case Keys.V:
                case Keys.W:
                case Keys.X:
                case Keys.Y:
                case Keys.Z:
                    Execute(new LetterCommand(this, keyData));
                    return true;
                case Keys.Enter:
                    Execute(new EnterCommand(this));
                    return true;
                case Keys.Back:
                    Execute(new BackSpaceCommand(this));
                    return true;
                case Keys.Oemtilde:
                case Keys.Oem7:
                case Keys.Oem6:
                case Keys.Delete:
                    return true;
                default:
                    Console.WriteLine(keyData.ToString());
                    break;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion

    }
}
