﻿namespace Workshop2.Command
{
    public class EnterCommand : KeyCommand
    {
        public EnterCommand(UndoRedoTextbox textbox) : base(textbox) { }

        public override bool Execute()
        {
            RestorePosition();
            Textbox.SelectedText = System.Environment.NewLine;
            return true;
        }

        public override bool Undo()
        {
            Textbox.SelectionStart = SelectionStart;
            Textbox.SelectionLength = 1;
            Textbox.SelectedText = SelectedText;
            return true;
        }
    }
}