﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workshop2.Command { 

    public interface ICommand
    {
        bool Execute();
        bool Undo();
    }
}
