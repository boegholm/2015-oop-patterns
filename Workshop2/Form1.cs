﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Workshop2.Observer.ClassicObserver;


namespace Workshop2
{
    public partial class Form1 : Form, IObserver
    {
//        ConsoleWriterObserver cw = new ConsoleWriterObserver();
//        ObservableStopWatch clock = new ObservableStopWatch();
        public Form1()
        {
            InitializeComponent();
            //clock.Attach(this);
            //clock.Attach(cw);
        }

        public void Update(ISubject theChangedSubject)
        {
            SetText(theChangedSubject.ToString());
        }

        private void SetText(string txt)
        {
            if(!IsDisposed)
                this.Invoke((Action)(() => label1.Text = txt));
        }

        private void btn_undo_Click(object sender, EventArgs e)
        {
            undoRedoTextbox1.Undo();
        }

        private void btn_redo_Click(object sender, EventArgs e)
        {
            undoRedoTextbox1.Redo();
        }
    }
}
