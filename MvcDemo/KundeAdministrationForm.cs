﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MvcDemo_Controller.Controllers;
using MvcDemo_Model.Models;

namespace MvcDemo
{
    public partial class KundeAdministrationForm : Form
    {
        //UI behavior (hvilke controls er enabled) befinder sig i viewet.         
        //En meget simpel UI tilstand: Er der valgt en kunde eller ej?
        //Hvis ja, skal det være muligt at redigere kunden, ellers ikke.
        private enum ViewState { IntetValgt, KundeValgt }
        
        //den 'primære' _Controller
        private KundeAdministrationController _Controller;
        
        private ViewState _ViewState; 

        public KundeAdministrationForm()
        {
            InitializeComponent();

            _Controller = new KundeAdministrationController();
            //View lytter til ændringer på Controller - kunne også have lyttet direkte på modellen.
            _Controller.CollectionChanged += controller_KundelisteÆndret;
            
            SetViewState(ViewState.IntetValgt);
        }

        //I denne løsning håndteres UI tilstande i View
        //(I ViewModel mønstret håndteres UI tilstande istedet i ViewModel)
        private void SetViewState(ViewState newState)
        {
            _ViewState = newState;
            switch (_ViewState)
            {
                case ViewState.IntetValgt:
                    fjernKundeButton.Enabled = false;
                    redigerKundeKontiButton.Enabled = false;
                    break;
                case ViewState.KundeValgt:
                    redigerKundeKontiButton.Enabled = true;
                    fjernKundeButton.Enabled = true;
                    break;
            }
        }

        private void controller_KundelisteÆndret(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OpdaterKundeVisning(); 
        }

        private void OpdaterKundeVisning()
        {
            IEnumerable<Kunde> kundeliste;
            if (visRigeKunderCheckBox.Checked)
                kundeliste = _Controller.VisRigeKunder();
            else
                kundeliste = _Controller.VisAlleKunder();

            //Udfyld listbox med kunder
            kunderListBox.Items.Clear();
            foreach (Kunde k in kundeliste)
            {
                kunderListBox.Items.Add(k);
            }
        }

        private void kunderListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (kunderListBox.Items.Count > 0 && kunderListBox.SelectedIndex >= 0)
                SetViewState(ViewState.KundeValgt);
            else
                SetViewState(ViewState.IntetValgt);

        }

        private void tilføjKundeButton_Click(object sender, EventArgs e)
        {
            SetViewState(ViewState.IntetValgt);

            OpretKundeForm opretKundeForm = new OpretKundeForm(_Controller);
            opretKundeForm.ShowDialog();            
        }

        private void redigerKundeKontiButton_Click(object sender, EventArgs e)
        {
            Kunde valgtKunde = (Kunde)kunderListBox.SelectedItem;
            RedigerKundeKontiForm redigerForm = new RedigerKundeKontiForm(valgtKunde);
            redigerForm.ShowDialog();            
        }

        private void fjernKundeButton_Click(object sender, EventArgs e)
        {
            Kunde valgtKunde = kunderListBox.SelectedItem as Kunde;
            DialogResult dialogResult = MessageBox.Show(string.Format("Vil du slette {0}?", valgtKunde.ToString()), "Bekræft sletning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                _Controller.SletKunde(valgtKunde);
                SetViewState(ViewState.IntetValgt);
            }            
        }

        private void visRigeKunderCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            SetViewState(ViewState.IntetValgt);
            OpdaterKundeVisning();
        }

        private void KundelisteForm_Activated(object sender, EventArgs e)
        {
            //Uden en aktiv model der signalerer tilstandsændringer er vi istedet nødt til i UI'en
            //at holde øje med hvorvidt der er sket ændringer i modellen
            //Herunder kaldes VisAlleKunder() hver gang denne for bliver aktiv
            //Dvs, koden kaldes hver gang vi returnerer fra TilføjKunde og FjernKunde
            //uanset om vi oprettede/fjernede eller blot sagde 'annuller'
            //VisAlleKunder(_Controller.VisAlleKunder());
        }       
    }
}
