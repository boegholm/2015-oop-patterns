﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MvcDemo_Controller.Controllers;


namespace MvcDemo
{
    public partial class OpretKontoForm : Form
    {
        private KundeKontiAdministrationController _Controller;
        private OpretKontoForm()
        {
            InitializeComponent();
        }

        //Dette view bruger samme _Controller som 'parent' viewet
        public OpretKontoForm(KundeKontiAdministrationController controller)
            : this()
        {
            this._Controller = controller;
        }

        private void gemKontoButton_Click(object sender, EventArgs e)
        {
            //Validering foretages - som et eksempel - her i controlleren. 
            //I meget simple tilfælde (som dette) kan man dog ligeså godt foretage validering
            //af controls' indhold i selve viewet.  
           
            int kontoNummer;
            decimal saldo;
            if (!int.TryParse(kontoNummerTextBox.Text, out kontoNummer))
                MessageBox.Show("Kontonummer er ikke et gyldigt heltal");
            else if (!decimal.TryParse(saldoTextBox.Text, out saldo))
                MessageBox.Show("Saldo er ikke et gyldigt tal");
            else 
            {
                _Controller.OpretKonto(kontoNummer, saldo);
                this.Close();
            }           
        }

        private void annullerButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
