﻿namespace MvcDemo
{
    partial class OpretKundeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navnTextBox = new System.Windows.Forms.TextBox();
            this.kundenummerTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gemKundeButton = new System.Windows.Forms.Button();
            this.annullerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // navnTextBox
            // 
            this.navnTextBox.Location = new System.Drawing.Point(97, 17);
            this.navnTextBox.Name = "navnTextBox";
            this.navnTextBox.Size = new System.Drawing.Size(100, 20);
            this.navnTextBox.TabIndex = 0;
            this.navnTextBox.TextChanged += new System.EventHandler(this.textBoxes_TextChanged);
            //this.navnTextBox.Leave += new System.EventHandler(this.textBoxes_Leave);
            // 
            // kundenummerTextBox
            // 
            this.kundenummerTextBox.Location = new System.Drawing.Point(97, 57);
            this.kundenummerTextBox.Name = "kundenummerTextBox";
            this.kundenummerTextBox.Size = new System.Drawing.Size(100, 20);
            this.kundenummerTextBox.TabIndex = 1;
            this.kundenummerTextBox.TextChanged += new System.EventHandler(this.textBoxes_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Navn:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kundenummer:";
            // 
            // gemKundeButton
            // 
            this.gemKundeButton.Location = new System.Drawing.Point(16, 107);
            this.gemKundeButton.Name = "gemKundeButton";
            this.gemKundeButton.Size = new System.Drawing.Size(75, 23);
            this.gemKundeButton.TabIndex = 4;
            this.gemKundeButton.Text = "Gem";
            this.gemKundeButton.UseVisualStyleBackColor = true;
            this.gemKundeButton.Click += new System.EventHandler(this.gemKundeButton_Click);
            // 
            // annullerButton
            // 
            this.annullerButton.Location = new System.Drawing.Point(97, 107);
            this.annullerButton.Name = "annullerButton";
            this.annullerButton.Size = new System.Drawing.Size(75, 23);
            this.annullerButton.TabIndex = 5;
            this.annullerButton.Text = "Annuller";
            this.annullerButton.UseVisualStyleBackColor = true;
            this.annullerButton.Click += new System.EventHandler(this.annullerButton_Click);
            // 
            // OpretKundeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 166);
            this.Controls.Add(this.annullerButton);
            this.Controls.Add(this.gemKundeButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kundenummerTextBox);
            this.Controls.Add(this.navnTextBox);
            this.Name = "OpretKundeForm";
            this.Text = "Opret Kunde";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox navnTextBox;
        private System.Windows.Forms.TextBox kundenummerTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button gemKundeButton;
        private System.Windows.Forms.Button annullerButton;
    }
}