﻿namespace MvcDemo
{
    partial class RedigerKundeKontiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.kundenummerTextBox = new System.Windows.Forms.TextBox();
            this.navnTextBox = new System.Windows.Forms.TextBox();
            this.kontiListBox = new System.Windows.Forms.ListBox();
            this.fjernKontoButton = new System.Windows.Forms.Button();
            this.tilføjKontoButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Kundenummer:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Navn:";
            // 
            // kundenummerTextBox
            // 
            this.kundenummerTextBox.Enabled = false;
            this.kundenummerTextBox.Location = new System.Drawing.Point(94, 52);
            this.kundenummerTextBox.Name = "kundenummerTextBox";
            this.kundenummerTextBox.Size = new System.Drawing.Size(100, 20);
            this.kundenummerTextBox.TabIndex = 7;
            // 
            // navnTextBox
            // 
            this.navnTextBox.Enabled = false;
            this.navnTextBox.Location = new System.Drawing.Point(94, 12);
            this.navnTextBox.Name = "navnTextBox";
            this.navnTextBox.Size = new System.Drawing.Size(100, 20);
            this.navnTextBox.TabIndex = 6;
            // 
            // kontiListBox
            // 
            this.kontiListBox.FormattingEnabled = true;
            this.kontiListBox.Location = new System.Drawing.Point(13, 93);
            this.kontiListBox.Name = "kontiListBox";
            this.kontiListBox.Size = new System.Drawing.Size(234, 95);
            this.kontiListBox.TabIndex = 12;
            this.kontiListBox.SelectedIndexChanged += new System.EventHandler(this.kontiListBox_SelectedIndexChanged);
            // 
            // fjernKontoButton
            // 
            this.fjernKontoButton.Location = new System.Drawing.Point(253, 122);
            this.fjernKontoButton.Name = "fjernKontoButton";
            this.fjernKontoButton.Size = new System.Drawing.Size(117, 23);
            this.fjernKontoButton.TabIndex = 14;
            this.fjernKontoButton.Text = "Fjern konto";
            this.fjernKontoButton.UseVisualStyleBackColor = true;
            this.fjernKontoButton.Click += new System.EventHandler(this.fjernKontoButton_Click);
            // 
            // tilføjKontoButton
            // 
            this.tilføjKontoButton.Location = new System.Drawing.Point(253, 93);
            this.tilføjKontoButton.Name = "tilføjKontoButton";
            this.tilføjKontoButton.Size = new System.Drawing.Size(117, 23);
            this.tilføjKontoButton.TabIndex = 13;
            this.tilføjKontoButton.Text = "Tilføj Konto";
            this.tilføjKontoButton.UseVisualStyleBackColor = true;
            this.tilføjKontoButton.Click += new System.EventHandler(this.tilføjKontoButton_Click);
            // 
            // RedigerKundeKontiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 206);
            this.Controls.Add(this.fjernKontoButton);
            this.Controls.Add(this.tilføjKontoButton);
            this.Controls.Add(this.kontiListBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kundenummerTextBox);
            this.Controls.Add(this.navnTextBox);
            this.Name = "RedigerKundeKontiForm";
            this.Text = "Rediger Konti";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kundenummerTextBox;
        private System.Windows.Forms.TextBox navnTextBox;
        private System.Windows.Forms.ListBox kontiListBox;
        private System.Windows.Forms.Button fjernKontoButton;
        private System.Windows.Forms.Button tilføjKontoButton;
    }
}