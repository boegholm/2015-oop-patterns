﻿namespace MvcDemo
{
    partial class OpretKontoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.annullerButton = new System.Windows.Forms.Button();
            this.gemKontoButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.saldoTextBox = new System.Windows.Forms.TextBox();
            this.kontoNummerTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // annullerButton
            // 
            this.annullerButton.Location = new System.Drawing.Point(93, 102);
            this.annullerButton.Name = "annullerButton";
            this.annullerButton.Size = new System.Drawing.Size(75, 23);
            this.annullerButton.TabIndex = 11;
            this.annullerButton.Text = "Annuller";
            this.annullerButton.UseVisualStyleBackColor = true;
            this.annullerButton.Click += new System.EventHandler(this.annullerButton_Click);
            // 
            // gemKontoButton
            // 
            this.gemKontoButton.Location = new System.Drawing.Point(12, 102);
            this.gemKontoButton.Name = "gemKontoButton";
            this.gemKontoButton.Size = new System.Drawing.Size(75, 23);
            this.gemKontoButton.TabIndex = 10;
            this.gemKontoButton.Text = "Gem";
            this.gemKontoButton.UseVisualStyleBackColor = true;
            this.gemKontoButton.Click += new System.EventHandler(this.gemKontoButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Angiv Saldo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Kontonummer:";
            // 
            // saldoTextBox
            // 
            this.saldoTextBox.Location = new System.Drawing.Point(93, 52);
            this.saldoTextBox.Name = "saldoTextBox";
            this.saldoTextBox.Size = new System.Drawing.Size(100, 20);
            this.saldoTextBox.TabIndex = 7;
            // 
            // kontoNummerTextBox
            // 
            this.kontoNummerTextBox.Location = new System.Drawing.Point(93, 12);
            this.kontoNummerTextBox.Name = "kontoNummerTextBox";
            this.kontoNummerTextBox.Size = new System.Drawing.Size(100, 20);
            this.kontoNummerTextBox.TabIndex = 6;
            // 
            // OpretKontoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.annullerButton);
            this.Controls.Add(this.gemKontoButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.saldoTextBox);
            this.Controls.Add(this.kontoNummerTextBox);
            this.Name = "OpretKontoForm";
            this.Text = "Opret Konto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button annullerButton;
        private System.Windows.Forms.Button gemKontoButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox saldoTextBox;
        private System.Windows.Forms.TextBox kontoNummerTextBox;
    }
}