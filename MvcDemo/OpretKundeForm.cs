﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MvcDemo_Controller.Controllers;

namespace MvcDemo
{
    public partial class OpretKundeForm : Form
    {
        //private OpretKundeController _Controller;
        private KundeAdministrationController _Controller;

        private OpretKundeForm()
        {
            InitializeComponent();
        }   
    
        public OpretKundeForm(KundeAdministrationController controller)
            : this()
        {
            this._Controller = controller;

            gemKundeButton.Enabled = false;
        }

        private void gemKundeButton_Click(object sender, EventArgs e)
        {
            try
            {
                _Controller.OpretKunde(navnTextBox.Text, kundenummerTextBox.Text);
                this.Close();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Ugyldigt input");
            }
        }

        private void annullerButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }        

        private void textBoxes_TextChanged(object sender, EventArgs e)
        {
            if (navnTextBox.Text != "" && kundenummerTextBox.Text != "")
                gemKundeButton.Enabled = true;
            else
                gemKundeButton.Enabled = false;
        }
    }
}
