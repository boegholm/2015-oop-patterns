﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MvcDemo_Controller.Controllers;
using MvcDemo_Model.Models;


namespace MvcDemo
{
    public partial class RedigerKundeKontiForm : Form
    {
        //Vores to simple tilstande
        private enum ViewState { IntetValgt, KontoValgt }
        
        private KundeKontiAdministrationController _Controller;
        private ViewState _ViewState;

        private RedigerKundeKontiForm()
        {
            InitializeComponent();            
        }

        public RedigerKundeKontiForm(Kunde aktuelKunde)
            : this()
        {
            _Controller = new KundeKontiAdministrationController(aktuelKunde);

            //I modsætning til kundelisten, så lytter dette view på signalering direkte fra modellaget.
            aktuelKunde.PropertyChanged += new PropertyChangedEventHandler(aktuelKunde_PropertyChanged);

            navnTextBox.Text = aktuelKunde.Navn;
            kundenummerTextBox.Text = aktuelKunde.KundeNummer;
            VisKonti(aktuelKunde.Konti);
            SetViewState(ViewState.IntetValgt);
        }

        void aktuelKunde_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Konti")
                VisKonti(_Controller.Kunde.Konti);
        }

        private void SetViewState(ViewState newState)
        {
            _ViewState = newState;
            switch (_ViewState)
            {
                case ViewState.IntetValgt:
                    fjernKontoButton.Enabled = false;
                    break;
                case ViewState.KontoValgt:
                    fjernKontoButton.Enabled = true;
                    break;
            }
        }

        private void VisKonti(IEnumerable<Konto> kontoliste)
        {
            kontiListBox.Items.Clear();
            foreach (Konto k in kontoliste)
            {
                kontiListBox.Items.Add(k);
            }
        }

        private void tilføjKontoButton_Click(object sender, EventArgs e)
        {
            //Vi sender controlleren videre til 'child' vinduet
            //da samme _Controller indeholder CRUD funktionalitet for en kundes konti
            OpretKontoForm opretKontoForm = new OpretKontoForm(_Controller);
            opretKontoForm.ShowDialog();
            SetViewState(ViewState.IntetValgt);
        }

        private void kontiListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (kontiListBox.Items.Count > 0 && kontiListBox.SelectedIndex >= 0)
                SetViewState(ViewState.KontoValgt);
            else
                SetViewState(ViewState.IntetValgt);
        }

        private void fjernKontoButton_Click(object sender, EventArgs e)
        {
            Konto valgtKonto = kontiListBox.SelectedItem as Konto;
            DialogResult dialogResult = MessageBox.Show(string.Format("Vil du slette {0}?", valgtKonto.ToString()), "Bekræft sletning", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                _Controller.SletKonto(valgtKonto);
                SetViewState(ViewState.IntetValgt);
            }  
        }
    }
}
