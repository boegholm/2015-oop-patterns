﻿namespace MvcDemo
{
    partial class KundeAdministrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kunderListBox = new System.Windows.Forms.ListBox();
            this.tilføjKundeButton = new System.Windows.Forms.Button();
            this.fjernKundeButton = new System.Windows.Forms.Button();
            this.visRigeKunderCheckBox = new System.Windows.Forms.CheckBox();
            this.redigerKundeKontiButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // kunderListBox
            // 
            this.kunderListBox.FormattingEnabled = true;
            this.kunderListBox.Location = new System.Drawing.Point(12, 12);
            this.kunderListBox.Name = "kunderListBox";
            this.kunderListBox.Size = new System.Drawing.Size(288, 95);
            this.kunderListBox.TabIndex = 0;
            this.kunderListBox.SelectedIndexChanged += new System.EventHandler(this.kunderListBox_SelectedIndexChanged);
            // 
            // tilføjKundeButton
            // 
            this.tilføjKundeButton.Location = new System.Drawing.Point(306, 12);
            this.tilføjKundeButton.Name = "tilføjKundeButton";
            this.tilføjKundeButton.Size = new System.Drawing.Size(117, 23);
            this.tilføjKundeButton.TabIndex = 1;
            this.tilføjKundeButton.Text = "Tilføj Kunde";
            this.tilføjKundeButton.UseVisualStyleBackColor = true;
            this.tilføjKundeButton.Click += new System.EventHandler(this.tilføjKundeButton_Click);
            // 
            // fjernKundeButton
            // 
            this.fjernKundeButton.Location = new System.Drawing.Point(306, 84);
            this.fjernKundeButton.Name = "fjernKundeButton";
            this.fjernKundeButton.Size = new System.Drawing.Size(117, 23);
            this.fjernKundeButton.TabIndex = 2;
            this.fjernKundeButton.Text = "Fjern kunde";
            this.fjernKundeButton.UseVisualStyleBackColor = true;
            this.fjernKundeButton.Click += new System.EventHandler(this.fjernKundeButton_Click);
            // 
            // visRigeKunderCheckBox
            // 
            this.visRigeKunderCheckBox.AutoSize = true;
            this.visRigeKunderCheckBox.Location = new System.Drawing.Point(12, 113);
            this.visRigeKunderCheckBox.Name = "visRigeKunderCheckBox";
            this.visRigeKunderCheckBox.Size = new System.Drawing.Size(117, 17);
            this.visRigeKunderCheckBox.TabIndex = 3;
            this.visRigeKunderCheckBox.Text = "Vis kun rige kunder";
            this.visRigeKunderCheckBox.UseVisualStyleBackColor = true;
            this.visRigeKunderCheckBox.CheckedChanged += new System.EventHandler(this.visRigeKunderCheckBox_CheckedChanged);
            // 
            // redigerKundeKontiButton
            // 
            this.redigerKundeKontiButton.Location = new System.Drawing.Point(306, 54);
            this.redigerKundeKontiButton.Name = "redigerKundeKontiButton";
            this.redigerKundeKontiButton.Size = new System.Drawing.Size(117, 23);
            this.redigerKundeKontiButton.TabIndex = 4;
            this.redigerKundeKontiButton.Text = "Redigér kundes konti";
            this.redigerKundeKontiButton.UseVisualStyleBackColor = true;
            this.redigerKundeKontiButton.Click += new System.EventHandler(this.redigerKundeKontiButton_Click);
            // 
            // KundelisteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 151);
            this.Controls.Add(this.redigerKundeKontiButton);
            this.Controls.Add(this.visRigeKunderCheckBox);
            this.Controls.Add(this.fjernKundeButton);
            this.Controls.Add(this.tilføjKundeButton);
            this.Controls.Add(this.kunderListBox);
            this.Name = "KundelisteForm";
            this.Text = "Kunde Administration";
            this.Activated += new System.EventHandler(this.KundelisteForm_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox kunderListBox;
        private System.Windows.Forms.Button tilføjKundeButton;
        private System.Windows.Forms.Button fjernKundeButton;
        private System.Windows.Forms.CheckBox visRigeKunderCheckBox;
        private System.Windows.Forms.Button redigerKundeKontiButton;
    }
}

