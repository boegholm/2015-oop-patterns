﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MvcDemo_Model.Models
{
    public class Konto : INotifyPropertyChanged
    {
        private decimal _Saldo;

        public Konto(int kontoNummer)
        {
            this.KontoNummer = kontoNummer;
        }

        public int KontoNummer { get; private set; } //readonly - kan ikke ændres
        public decimal Saldo
        {
            get { return _Saldo; }
            set 
            {
                if (value != _Saldo)
                {
                    _Saldo = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Saldo"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        public override string ToString()
        {
            return string.Format("Konto '{0}' med saldo {1} kr.", this.KontoNummer, this.Saldo);
        }
    }
}
