﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace MvcDemo_Model.Models
{
    public class Kunde : INotifyPropertyChanged
    {
        private string _Navn;

        public string Navn 
        {
            get { return _Navn; }
            set
            {
                if (value != _Navn)
                {
                    this._Navn = value;
                    OnNotifyPropertyChanged(new PropertyChangedEventArgs("Navn"));
                }
            }
        }
        public string KundeNummer { get; private set; }
        private List<Konto> _Konti;
        
        public Kunde(string navn, string kundenummer)
        {
            //validering udeladt
            this.Navn = navn;
            this.KundeNummer = kundenummer;
            this._Konti = new List<Konto>();
        }
        
        public IEnumerable<Konto> Konti
        {
            get
            {
                foreach (Konto k in _Konti)
                    yield return k;
            }
        }

        public void TilføjKonto(Konto nyKonto)
        {
            _Konti.Add(nyKonto);
            OnNotifyPropertyChanged(new PropertyChangedEventArgs("Konti"));
        }

        public bool FjernKonto(Konto eksisterendeKonto)
        {
            bool result = _Konti.Remove(eksisterendeKonto);
            OnNotifyPropertyChanged(new PropertyChangedEventArgs("Konti"));
            return result;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnNotifyPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

        public override string ToString()
        {
            return this.Navn + ", Kundenummer: " + this.KundeNummer;
        }

        
    }
}
