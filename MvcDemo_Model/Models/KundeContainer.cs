﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace MvcDemo_Model.Models
{
    //NB: Der er praktisk talt ingen validering i denne klasse (for at holde den kort)
    public class KundeContainer : ICollection<Kunde>, INotifyCollectionChanged
    {
        private Dictionary<string, Kunde> _Kunder;

        #region Singleton implementation
        private static KundeContainer _Instance;
        private KundeContainer()
        {
            _Kunder = new Dictionary<string, Kunde>();
        }

        public static KundeContainer Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new KundeContainer();

                return _Instance;
            }
        }
        #endregion

        public void Add(Kunde kunde)
        {
            if (_Kunder.ContainsKey(kunde.KundeNummer))
                throw new ArgumentException("Kunde med angivne kunnenummer findes allerede");

            _Kunder.Add(kunde.KundeNummer, kunde);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, kunde));
        }

        public void Clear()
        {
            _Kunder.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(Kunde kunde)
        {
            return _Kunder.ContainsKey(kunde.KundeNummer); 
        }

        public bool ContainsKundeWithKundenummer(string kundeNummer)
        {
            return _Kunder.ContainsKey(kundeNummer);
        }

        public Kunde this[string kundeNummer]
        {
            get { return _Kunder[kundeNummer]; }
        }

        public void CopyTo(Kunde[] array, int arrayIndex)
        {
            _Kunder.Values.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _Kunder.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Kunde kunde)
        {
            bool result = _Kunder.Remove(kunde.KundeNummer);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, kunde));
            return result;
        }

        public IEnumerator<Kunde> GetEnumerator()
        {
            foreach (Kunde k in _Kunder.Values)
                yield return k;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanged != null)
                CollectionChanged(this, args);
        }        
    }
}
